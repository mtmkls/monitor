
// monitor Linux netlink activity
//
// license: WTFPL
//
// gcc -Wall -Wextra -Wshadow -Wstrict-prototypes -Wmissing-declarations -Wwrite-strings -Wvla -Wc++-compat -g monitor.c -o monitor

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/socket.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netinet/ether.h>
#include <linux/rtnetlink.h>

// linux/neighbor.h should have this...
#ifndef NDM_RTA
#define NDM_RTA(r) ((struct rtattr*)(((char*)(r)) + NLMSG_ALIGN(sizeof(struct ndmsg))))
#endif

static struct rtattr *find_rtattr(struct rtattr *attr, unsigned len, unsigned type)
{
    while (RTA_OK(attr, len)) {
        if (attr->rta_type == type) {
            return attr;
        }
        attr = RTA_NEXT(attr, len);
    }
    return NULL;
}

static const char *get_typename(unsigned char type)
{
    switch (type) {
        case RTN_UNSPEC: return "unspec";
        case RTN_UNICAST: return "unicast";
        case RTN_LOCAL: return "local";
        case RTN_BROADCAST: return "broadcast";
        case RTN_ANYCAST: return "anycast";
        case RTN_MULTICAST: return "multicast";
        case RTN_BLACKHOLE: return "blackhole";
        case RTN_UNREACHABLE: return "unreachable";
        case RTN_PROHIBIT: return "prohibit";
        case RTN_THROW: return "not this table";
        case RTN_NAT: return "nat";
        case RTN_XRESOLVE: return "external";
    }
    return "unknown";
}

int main(void)
{
    int sock = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    if (sock < 0) {
        perror("could not create netlink socket");
        return EXIT_FAILURE;
    }

    struct sockaddr_nl  addr;
    memset(&addr, 0, sizeof(addr));

    addr.nl_family = AF_NETLINK;
    addr.nl_groups = RTMGRP_LINK | RTMGRP_NEIGH |
        RTMGRP_IPV4_IFADDR | RTMGRP_IPV4_ROUTE |
        RTMGRP_IPV6_IFADDR | RTMGRP_IPV6_ROUTE;
    // some people say that nl_pid should be set to a unique number (getpid() and thread id),
    //   but man 7 netlink says it's best to leave it as 0

    if (bind(sock, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
        perror("could not bind netlink socket");
        close(sock);
        return EXIT_FAILURE;
    }

    char recvbuf[8192]
        __attribute__ ((aligned(__alignof__(struct nlmsghdr))));

    while (1) {
        int err = recv(sock, recvbuf, sizeof(recvbuf), 0);

        if (err < 0) {
            perror("recvmsg");
            close(sock);
            return EXIT_FAILURE;
        }
        if (err == 0) continue; // this should never happen though
        unsigned recvlen = err;

        printf("received %u bytes\n", recvlen);

        // message layout: an `nlmsghdr` header, a message-specific struct, and an array of `rtattr` attribute TLV
        // here we only decode the most interesting properties and attributes

        for (struct nlmsghdr *nh=(struct nlmsghdr *)recvbuf; NLMSG_OK(nh, recvlen); nh=NLMSG_NEXT(nh, recvlen)) {
            printf("nlmsg type 0x%x %u len %u flags 0x%x\n",
                    nh->nlmsg_type, nh->nlmsg_type, nh->nlmsg_len, nh->nlmsg_flags);

            if (nh->nlmsg_type == NLMSG_DONE) {
                //printf("NLMSG_DONE\n"); // we get this in multipart reply (=never)
                break;
            } else if (nh->nlmsg_type == NLMSG_ERROR) {
                struct nlmsgerr *nlerr = (struct nlmsgerr *)NLMSG_DATA(nh);
                fprintf(stderr, "\033[31mRTNETLINK ERROR\033[0m %s\n", strerror(-nlerr->error));
                // return EXIT_FAILURE;
            } else if (nh->nlmsg_type == RTM_NEWLINK || nh->nlmsg_type == RTM_DELLINK) {
                //TODO when do we get RTM_DELLINK?
                const char *msgtype = nh->nlmsg_type == RTM_NEWLINK ? "new" : "del";
                struct ifinfomsg *ifi = (struct ifinfomsg*) NLMSG_DATA(nh);

                printf("\033[32m%s link\033[0m type %u index %d flags 0x%x change %u\n",
                        msgtype, ifi->ifi_type, ifi->ifi_index, ifi->ifi_flags, ifi->ifi_change);
                if (ifi->ifi_flags & IFF_UP) printf("    UP\n");
                if (ifi->ifi_flags & IFF_RUNNING) printf("    RUNNING\n");

                struct rtattr *ifname_attr = find_rtattr(IFLA_RTA(ifi), nh->nlmsg_len, IFLA_IFNAME);
                if (ifname_attr) {
                    printf("  interface name '%s'\n", (char*)RTA_DATA(ifname_attr));
                } else {
                    printf("  no interface name specified\n");
                }
            } else if (nh->nlmsg_type == RTM_NEWADDR || nh->nlmsg_type == RTM_DELADDR) {
                const char *msgtype = nh->nlmsg_type == RTM_NEWADDR ? "new" : "del";
                struct ifaddrmsg *ifa = (struct ifaddrmsg*)NLMSG_DATA(nh);
                const char *family = ifa->ifa_family==AF_INET?"IPv4":ifa->ifa_family==AF_INET6?"IPv6":"unknown";
                const char *scope = ifa->ifa_scope==RT_SCOPE_LINK?"Link":ifa->ifa_scope==RT_SCOPE_UNIVERSE?"Global":"unknown";
                char ifname[IF_NAMESIZE] = "";
                if_indextoname(ifa->ifa_index, ifname); // or ioctl(SIOCGIFNAME) (man 7 netdevice)

                printf("\033[33m%s address\033[0m family %s prefixlen %u scope %u %s index %d (%s) flags 0x%x\n",
                        msgtype, family, ifa->ifa_prefixlen, ifa->ifa_scope, scope,
                        ifa->ifa_index, ifname, ifa->ifa_flags);

                int flags = ifa->ifa_flags;
                struct rtattr *flags_attr = find_rtattr(IFA_RTA(ifa), nh->nlmsg_len, IFA_FLAGS);
                if (flags_attr) {
                    flags = ((int*)RTA_DATA(flags_attr))[0];
                    printf("   flags attribute 0x%x\n", flags);
                }
                if (flags & IFA_F_PERMANENT) printf("    Permanent\n"); // means not autoconfigured
                if (flags & IFA_F_TENTATIVE) printf("    Tentative\n"); // means duplicate detection is ongoing
                if (flags & IFA_F_TEMPORARY) printf("    Temporary\n"); // see RFC 4941
                if (flags & IFA_F_MANAGETEMPADDR) printf("    Manage Temp Addr\n"); // means it's a template for temporary

                struct rtattr *addr_attr = find_rtattr(IFA_RTA(ifa), nh->nlmsg_len, IFA_ADDRESS);
                if (addr_attr) {
                    char str[INET6_ADDRSTRLEN];
                    if (inet_ntop(ifa->ifa_family, RTA_DATA(addr_attr), str, sizeof(str)) != NULL) {
                        printf("  address %s / %u\n", str, ifa->ifa_prefixlen);
                    } else {
                        printf("  failed to decode address\n");
                    }
                } else {
                    printf("  no address specified\n");
                }
                //TODO the interesting ones are Global, not Tentative
            } else if (nh->nlmsg_type == RTM_NEWROUTE || nh->nlmsg_type == RTM_DELROUTE) {
                const char *msgtype = nh->nlmsg_type == RTM_NEWROUTE ? "new" : "del";
                struct rtmsg *rt = (struct rtmsg*)NLMSG_DATA(nh);
                const char *family = rt->rtm_family==AF_INET?"IPv4":rt->rtm_family==AF_INET6?"IPv6":"unknown";
                const char *protocol = "daemon";
                switch (rt->rtm_protocol) {
                    case RTPROT_UNSPEC: protocol = "unspec"; break;
                    case RTPROT_REDIRECT: protocol = "ICMP redirect"; break;
                    case RTPROT_KERNEL: protocol = "kernel"; break;
                    case RTPROT_BOOT: protocol = "boot"; break;
                    case RTPROT_STATIC: protocol = "static"; break;
                }
                const char *type = get_typename(rt->rtm_type);

                printf("\033[34m%s route\033[0m family %s dstlen %u srclen %u protocol %u (%s) type %u (%s)\n",
                        msgtype, family, rt->rtm_dst_len, rt->rtm_src_len,
                        rt->rtm_protocol, protocol, rt->rtm_type, type);

                struct rtattr *src_attr = find_rtattr(RTM_RTA(rt), nh->nlmsg_len, RTA_SRC);
                if (src_attr) {
                    char str[INET6_ADDRSTRLEN];
                    if (inet_ntop(rt->rtm_family, RTA_DATA(src_attr), str, sizeof(str)) != NULL) {
                        printf("  source %s / %u\n", str, rt->rtm_src_len);
                    } else {
                        printf("  failed to decode source address\n");
                    }
                }
                struct rtattr *dst_attr = find_rtattr(RTM_RTA(rt), nh->nlmsg_len, RTA_DST);
                if (dst_attr) {
                    char str[INET6_ADDRSTRLEN];
                    if (inet_ntop(rt->rtm_family, RTA_DATA(dst_attr), str, sizeof(str)) != NULL) {
                        printf("  destination %s / %u\n", str, rt->rtm_dst_len);
                    } else {
                        printf("  failed to decode destination address\n");
                    }
                }
                struct rtattr *iif_attr = find_rtattr(RTM_RTA(rt), nh->nlmsg_len, RTA_IIF);
                if (iif_attr) {
                    char ifname[IF_NAMESIZE] = "";
                    if_indextoname(((int*)RTA_DATA(iif_attr))[0], ifname);
                    printf("  incoming interface %d (%s)\n", ((int*)RTA_DATA(iif_attr))[0], ifname);
                }
                struct rtattr *oif_attr = find_rtattr(RTM_RTA(rt), nh->nlmsg_len, RTA_OIF);
                if (oif_attr) {
                    char ifname[IF_NAMESIZE] = "";
                    if_indextoname(((int*)RTA_DATA(oif_attr))[0], ifname);
                    printf("  outgoing interface %d (%s)\n", ((int*)RTA_DATA(oif_attr))[0], ifname);
                }
                struct rtattr *gw_attr = find_rtattr(RTM_RTA(rt), nh->nlmsg_len, RTA_GATEWAY);
                if (gw_attr) {
                    char str[INET6_ADDRSTRLEN];
                    if (inet_ntop(rt->rtm_family, RTA_DATA(gw_attr), str, sizeof(str)) != NULL) {
                        printf("  gateway %s\n", str);
                    } else {
                        printf("  failed to decode gateway address\n");
                    }
                }
            } else if (nh->nlmsg_type == RTM_NEWNEIGH || nh->nlmsg_type == RTM_DELNEIGH) {
                const char *msgtype = nh->nlmsg_type == RTM_NEWNEIGH ? "new" : "del";
                struct ndmsg *nd = (struct ndmsg*)NLMSG_DATA(nh);
                const char *family = nd->ndm_family==AF_INET?"IPv4":nd->ndm_family==AF_INET6?"IPv6":"unknown";
                char ifname[IF_NAMESIZE] = "";
                if_indextoname(nd->ndm_ifindex, ifname);
                const char *type = get_typename(nd->ndm_type);

                printf("\033[35m%s neighbor\033[0m family %s index %d (%s) type %s\n",
                        msgtype, family, nd->ndm_ifindex, ifname, type);
                if (nd->ndm_state & NUD_INCOMPLETE) printf("    Incomplete\n");
                if (nd->ndm_state & NUD_REACHABLE)  printf("    Reachable\n");
                if (nd->ndm_state & NUD_STALE)      printf("    Stale\n");
                if (nd->ndm_state & NUD_DELAY)      printf("    Delay\n");
                if (nd->ndm_state & NUD_PROBE)      printf("    Probe\n");
                if (nd->ndm_state & NUD_FAILED)     printf("    Failed\n");
                if (nd->ndm_state & NUD_NOARP)      printf("    NoARP\n");
                if (nd->ndm_state & NUD_PERMANENT)  printf("    Permanent\n");

                struct rtattr *dst_attr = find_rtattr(NDM_RTA(nd), nh->nlmsg_len, NDA_DST);
                if (dst_attr) {
                    char str[INET6_ADDRSTRLEN];
                    if (inet_ntop(nd->ndm_family, RTA_DATA(dst_attr), str, sizeof(str)) != NULL) {
                        printf("  destination %s\n", str);
                    } else {
                        printf("  failed to decode destination address\n");
                    }
                }
                struct rtattr *ll_attr = find_rtattr(NDM_RTA(nd), nh->nlmsg_len, NDA_LLADDR);
                if (ll_attr) {
                    printf("  MAC %s\n", ether_ntoa((struct ether_addr *)RTA_DATA(ll_attr)));
                }
            }
        }
    }

    return EXIT_SUCCESS;
}
